<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Enums\UserType;
use App\Models\Lead;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        User::factory()->create([
            'name' => 'manager',
            'email' => 'manager@mail.ru',
            'type' => UserType::Manager,
        ]);

        User::factory(10)
            ->has(
                Lead::factory()
                    ->count(rand(1, 4))
                    ->for(User::factory())
            )
            ->create();
    }
}
