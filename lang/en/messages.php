<?php

return [
    'model_not_found' => 'Entity not found',
    'route_not_found' => 'Route not found',
    'auth_error' => 'Unauthorized',
    'lead_not_found' => 'Lead not found',
    'bad_request' => 'Bad request',
    'invalid_credentials' => 'Invalid user credentials',
];
