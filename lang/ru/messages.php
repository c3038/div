<?php

return [
    'model_not_found' => 'Не удалось найти этот элемент в базе данных',
    'route_not_found' => 'Маршрут не найден',
    'auth_error' => 'Требуется авторизация',
    'lead_not_found' => 'Лид не найден',
    'bad_request' => 'Не верный запрос',
    'invalid_credentials' => 'Не верные параметры пользователя для входа',
];
