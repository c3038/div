help: ## Помощь
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

up: ## Старт контейнера
	docker-compose up -d
down: ## Остановка контейнера
	docker-compose down
build: ## Сборка проекта
	cp .env.example .env
	docker-compose build
	docker-compose up -d
	docker-compose exec app composer install
	docker-compose exec app php artisan key:generate
	docker-compose exec app php artisan migrate
	docker-compose exec app php artisan db:seed
	docker-compose exec app php artisan l5-swagger:generate
	docker-compose exec app php artisan event:clear
	docker-compose exec app php artisan event:cache
rebuild: ## Пересобрать проект
	docker-compose down
	docker-compose build
	docker-compose up -d
