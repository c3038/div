<?php

namespace App\Http\Controllers\Swagger;

use App\Http\Controllers\Controller;
use OpenApi\Annotations as OA;

/**
 * @OA\Post(
 *     path="/api/v1/leads",
 *     summary="Created",
 *     tags={"Lead"},
 *     security={{ "bearerAuth": {} }},
 *
 *     @OA\RequestBody(
 *
 *          @OA\JsonContent(
 *              allOf={
 *
 *                  @OA\Schema(
 *                      required={"message"},
 *
 *                      @OA\Property(
 *                          property="message",
 *                          type="string",
 *                          example="Some new message",
 *                      ),
 *                  )
 *              }
 *          )
 *     ),
 *
 *     @OA\Response(
 *         response="201",
 *         description="Created",
 *
 *         @OA\JsonContent(
 *
 *             @OA\Property(
 *                 property="id",
 *                 type="string",
 *                 example="9a25b94c-e752-4702-b1cb-f29e9309c240",
 *             ),
 *             @OA\Property(
 *                 property="userName",
 *                 type="string",
 *                 example="Charles Herman",
 *             ),
 *             @OA\Property(
 *                 property="email",
 *                 type="string",
 *                 example="dan43@example.org"
 *             ),
 *             @OA\Property(
 *                 property="type",
 *                 type="string",
 *                 enum={"User", "Manager"},
 *                 example="user"
 *             ),
 *             @OA\Property(
 *                 property="status",
 *                 type="string",
 *                 enum={"Active", "Resolved"},
 *                 example="active"
 *             ),
 *             @OA\Property(
 *                 property="message",
 *                 type="string",
 *                 example="Some new message"
 *             ),
 *             @OA\Property(
 *                 property="comment",
 *                 type="string",
 *                 example=""
 *             ),
 *             @OA\Property(
 *                 property="created_at",
 *                 type="string",
 *                 example="15-09-2023"
 *             ),
 *             @OA\Property(
 *                 property="updated_at",
 *                 type="string",
 *                 example="15-09-2023"
 *             ),
 *         )
 *     )
 * ),
 *
 * @OA\Get(
 *     path="/api/v1/leads",
 *     summary="Index",
 *     tags={"Lead"},
 *     security={{ "bearerAuth": {} }},
 *
 *     @OA\Parameter(
 *         description="status of leads",
 *         in="query",
 *         required=false,
 *         name="_status",
 *
 *         @OA\Schema(
 *             type="string",
 *             enum={"active", "resolved"},
 *             example="active"
 *         ),
 *     ),
 *
 *     @OA\Response(
 *         response="200",
 *         description="OK",
 *
 *         @OA\JsonContent(
 *
 *             @OA\Property(
 *                 property="countRecords",
 *                 type="integer",
 *                 example="43"
 *             ),
 *             @OA\Property(
 *                 property="limit",
 *                 type="integer",
 *                 example="20"
 *             ),
 *             @OA\Property(
 *                 property="offset",
 *                 type="integer",
 *                 example="0"
 *             ),
 *             @OA\Property(
 *                 property="leads",
 *                 type="array",
 *
 *                 @OA\Items(
 *
 *                     @OA\Property(
 *                         property="id",
 *                         type="string",
 *                         example="9a25b94c-e752-4702-b1cb-f29e9309c240",
 *                     ),
 *                     @OA\Property(
 *                         property="userName",
 *                         type="string",
 *                         example="Charles Herman",
 *                     ),
 *                     @OA\Property(
 *                         property="email",
 *                         type="string",
 *                         example="dan43@example.org"
 *                     ),
 *                     @OA\Property(
 *                         property="type",
 *                         type="string",
 *                         enum={"User", "Manager"},
 *                         example="user"
 *                     ),
 *                     @OA\Property(
 *                         property="status",
 *                         type="string",
 *                         enum={"active", "resolved"},
 *                         example="active"
 *                     ),
 *                     @OA\Property(
 *                         property="message",
 *                         type="string",
 *                         example="Some new message"
 *                     ),
 *                     @OA\Property(
 *                         property="comment",
 *                         type="string",
 *                         example=""
 *                     ),
 *                     @OA\Property(
 *                         property="created_at",
 *                         type="string",
 *                         example="15-09-2023"
 *                     ),
 *                     @OA\Property(
 *                         property="updated_at",
 *                         type="string",
 *                         example="15-09-2023"
 *                     ),
 *                 ),
 *             ),
 *         )
 *     )
 * ),
 *
 * @OA\Get(
 *     path="/api/v1/leads/{lead}",
 *     summary="Show",
 *     tags={"Lead"},
 *     security={{ "bearerAuth": {} }},
 *
 *     @OA\Parameter(
 *         description="ID lead",
 *         in="path",
 *         name="lead",
 *         required=true,
 *         example="9a25b94c-e752-4702-b1cb-f29e9309c240"
 *     ),
 *
 *     @OA\Response(
 *         response="200",
 *         description="OK",
 *
 *         @OA\JsonContent(
 *
 *             @OA\Property(
 *                 property="id",
 *                 type="string",
 *                 example="9a25b94c-e752-4702-b1cb-f29e9309c240",
 *             ),
 *             @OA\Property(
 *                 property="userName",
 *                 type="string",
 *                 example="Charles Herman",
 *             ),
 *             @OA\Property(
 *                 property="email",
 *                 type="string",
 *                 example="dan43@example.org"
 *             ),
 *             @OA\Property(
 *                 property="type",
 *                 type="string",
 *                 enum={"User", "Manager"},
 *                 example="user"
 *             ),
 *             @OA\Property(
 *                 property="status",
 *                 type="string",
 *                 enum={"active", "resolved"},
 *                 example="active"
 *             ),
 *             @OA\Property(
 *                 property="message",
 *                 type="string",
 *                 example="Some new message"
 *             ),
 *             @OA\Property(
 *                 property="comment",
 *                 type="string",
 *                 example=""
 *             ),
 *             @OA\Property(
 *                 property="created_at",
 *                 type="string",
 *                 example="15-09-2023"
 *             ),
 *             @OA\Property(
 *                 property="updated_at",
 *                 type="string",
 *                 example="15-09-2023"
 *             ),
 *         )
 *     )
 * ),
 *
 * @OA\Patch(
 *     path="/api/v1/leads/{lead}",
 *     summary="Update",
 *     tags={"Lead"},
 *     security={{ "bearerAuth": {} }},
 *
 *     @OA\Parameter(
 *         description="ID lead",
 *         in="path",
 *         name="lead",
 *         required=true,
 *         example="9a25b94c-e752-4702-b1cb-f29e9309c240"
 *     ),
 *
 *     @OA\RequestBody(
 *
 *          @OA\JsonContent(
 *              allOf={
 *
 *                  @OA\Schema(
 *                      required={"status", "comment"},
 *
 *                      @OA\Property(
 *                          property="status",
 *                          type="string",
 *                          enum={"active", "resolved"},
 *                          example="resolved"
 *                      ),
 *                      @OA\Property(
 *                          property="comment",
 *                          type="string",
 *                          example="Some comment updete"
 *                      ),
 *                  )
 *              }
 *          )
 *     ),
 *
 *     @OA\Response(
 *         response="200",
 *         description="OK",
 *
 *         @OA\JsonContent(
 *
 *             @OA\Property(
 *                 property="id",
 *                 type="string",
 *                 example="9a25b94c-e752-4702-b1cb-f29e9309c240",
 *             ),
 *             @OA\Property(
 *                 property="userName",
 *                 type="string",
 *                 example="Charles Herman",
 *             ),
 *             @OA\Property(
 *                 property="email",
 *                 type="string",
 *                 example="dan43@example.org"
 *             ),
 *             @OA\Property(
 *                 property="type",
 *                 type="string",
 *                 enum={"User", "Manager"},
 *                 example="user"
 *             ),
 *             @OA\Property(
 *                 property="status",
 *                 type="string",
 *                 enum={"active", "resolved"},
 *                 example="resolved"
 *             ),
 *             @OA\Property(
 *                 property="message",
 *                 type="string",
 *                 example="Some new message"
 *             ),
 *             @OA\Property(
 *                 property="comment",
 *                 type="string",
 *                 example="Some comment updete"
 *             ),
 *             @OA\Property(
 *                 property="created_at",
 *                 type="string",
 *                 example="15-09-2023"
 *             ),
 *             @OA\Property(
 *                 property="updated_at",
 *                 type="string",
 *                 example="15-09-2023"
 *             ),
 *         )
 *     )
 * ),
 */
class LeadController extends Controller
{
}
