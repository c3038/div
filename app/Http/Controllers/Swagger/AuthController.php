<?php

declare(strict_types=1);

namespace App\Http\Controllers\Swagger;

use App\Http\Controllers\Controller;
use OpenApi\Annotations as OA;

/**
 * @OA\Post(
 *     path="/api/v1/auth/login",
 *     summary="Login",
 *     tags={"Auth"},
 *
 *     @OA\RequestBody(
 *
 *          @OA\JsonContent(
 *              allOf={
 *
 *                  @OA\Schema(
 *                      required={"email", "password"},
 *
 *                      @OA\Property(
 *                          property="email",
 *                          type="string",
 *                          example="manager@mail.ru",
 *                      ),
 *                      @OA\Property(
 *                          property="password",
 *                          type="string",
 *                          example="password"
 *                      ),
 *                  )
 *              }
 *          )
 *     ),
 *
 *     @OA\Response(
 *         response="200",
 *         description="OK",
 *
 *         @OA\JsonContent(
 *
 *             @OA\Property(
 *                 property="token",
 *                 type="string",
 *                 example="1|laravel_sanctum_Kv1Xj9zX6ZdhDh1cZkmmeBLMOOTxlQsqn5wBZVtk0890c242",
 *             ),
 *         )
 *     )
 * ),
 *
 * @OA\Post(
 *     path="/api/v1/auth/register",
 *     summary="Register",
 *     tags={"Auth"},
 *
 *     @OA\RequestBody(
 *
 *          @OA\JsonContent(
 *              allOf={
 *
 *                  @OA\Schema(
 *                      required={"name", "login", "email", "password"},
 *
 *                      @OA\Property(
 *                          property="name",
 *                          type="string",
 *                          example="manager",
 *                      ),
 *                      @OA\Property(
 *                          property="email",
 *                          type="string",
 *                          example="manager@mail.ru",
 *                      ),
 *                      @OA\Property(
 *                          property="type",
 *                          type="string",
 *                          enum={"User", "Manager"},
 *                          example="manager"
 *                      ),
 *                      @OA\Property(
 *                          property="password",
 *                          type="string",
 *                          example="password"
 *                      ),
 *                      @OA\Property(
 *                          property="password_confirmation",
 *                          type="string",
 *                          example="password"
 *                      ),
 *                  )
 *              }
 *          )
 *     ),
 *
 *     @OA\Response(
 *         response="201",
 *         description="Created",
 *
 *         @OA\JsonContent(
 *
 *            @OA\Property(
 *                 property="id",
 *                 type="sttring",
 *                 example="9a25b94c-e752-4702-b1cb-f29e9309c240",
 *             ),
 *             @OA\Property(
 *                 property="name",
 *                 type="string",
 *                 example="manager",
 *             ),
 *             @OA\Property(
 *                 property="email",
 *                 type="string",
 *                 example="manager@mail.ru",
 *             ),
 *             @OA\Property(
 *                 property="type",
 *                 type="string",
 *                 enum={"User", "Manager"},
 *                 example="manager"
 *             ),
 *         )
 *     )
 * ),
 */
class AuthController extends Controller
{
}
