<?php

namespace App\Http\Controllers\Auth;

use App\Facades\AuthFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {
        return AuthFacade::store($request->data());
    }

    public function login(LoginRequest $request)
    {
        return AuthFacade::login($request->data());
    }
}
