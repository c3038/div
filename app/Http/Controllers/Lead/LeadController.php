<?php

namespace App\Http\Controllers\Lead;

use App\Enums\UserType;
use App\Facades\LeadFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\Lead\StoreRequest;
use App\Http\Requests\Lead\UpdateRequest;
use App\Http\Resources\Lead\LeadResource;
use App\Models\Lead;
use App\Services\Lead\Dto\IndexLeadDto;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class LeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
        $this->middleware(sprintf('abilities:is:%s', UserType::Manager()))->only(['index', 'update']);
    }

    public function store(StoreRequest $request): JsonResponse
    {
        return response()->json(
            new LeadResource(LeadFacade::store($request->data())),
            Response::HTTP_CREATED
        );
    }

    public function index(Request $request): IndexLeadDto
    {
        return LeadFacade::index($request->toArray());
    }

    public function show(Lead $lead): LeadResource
    {
        return new LeadResource($lead);
    }

    public function update(UpdateRequest $request): LeadResource
    {
        return new LeadResource(LeadFacade::update($request->data()));
    }
}
