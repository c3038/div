<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\ApiRequest;
use App\Services\Auth\Dto\LoginDto;

class LoginRequest extends ApiRequest
{
    public function rules(): array
    {
        return [
            'email' => ['nullable', 'email', 'max:255'],
            'password' => ['required'],
        ];
    }

    public function data(): LoginDto
    {
        return LoginDto::from($this->validated());
    }
}
