<?php

namespace App\Http\Requests\Auth;

use App\Enums\UserType;
use App\Services\Auth\Dto\RegisterUserData;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'unique:users,email', 'email', 'max:254'],
            'password' => ['required', 'confirmed'],
            'type' => [Rule::in([UserType::User, UserType::Manager])],
        ];
    }

    public function data(): RegisterUserData
    {
        return RegisterUserData::from($this->validated());
    }
}
