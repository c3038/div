<?php

namespace App\Http\Requests\Lead;

use App\Enums\LeadStatus;
use App\Http\Requests\ApiRequest;
use App\Services\Lead\Dto\UpdateLeadDto;
use Illuminate\Validation\Rule;

class UpdateRequest extends ApiRequest
{
    public function getId(): string
    {
        return $this->route('lead');
    }

    protected function prepareForValidation(): void
    {
        $this->merge(['lead' => $this->getId()]);
    }

    public function rules(): array
    {
        return [
            'comment' => ['required', 'string'],
            'status' => ['required', Rule::in([LeadStatus::Active, LeadStatus::Resolved])],
        ];
    }

    public function data(): UpdateLeadDto
    {
        return UpdateLeadDto::from([
            'id' => $this->getId(),
            ...$this->validated(),
        ]);
    }
}
