<?php

namespace App\Http\Requests\Lead;

use App\Http\Requests\ApiRequest;
use App\Services\Lead\Dto\CreateLeadDto;

class StoreRequest extends ApiRequest
{
    public function rules(): array
    {
        return [
            'message' => ['required', 'string'],
        ];
    }

    public function data(): CreateLeadDto
    {
        return CreateLeadDto::from($this->validated());
    }
}
