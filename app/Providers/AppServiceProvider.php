<?php

namespace App\Providers;

use App\Http\Resources\Lead\LeadResource;
use App\Services\Auth\AuthService;
use App\Services\Lead\LeadService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(AuthService::class, AuthService::class);
        $this->app->bind(LeadService::class, LeadService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        LeadResource::withoutWrapping();
    }
}
