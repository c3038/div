<?php

declare(strict_types=1);

namespace App\Facades;

use App\Models\Lead;
use App\Services\Lead\Dto\CreateLeadDto;
use App\Services\Lead\Dto\IndexLeadDto;
use App\Services\Lead\Dto\UpdateLeadDto;
use App\Services\Lead\LeadService;
use Illuminate\Support\Facades\Facade;

/**
 * @method static Lead store(CreateLeadDto $data)
 * @method static IndexLeadDto index(array $queryString)
 * @method static Lead update(UpdateLeadDto $data)
 *
 * @see LeadService
 */
class LeadFacade extends Facade
{
    public static function getFacadeAccessor(): string
    {
        return LeadService::class;
    }
}
