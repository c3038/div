<?php

declare(strict_types=1);

namespace App\Facades;

use App\Http\Resources\Auth\AuthUserResource;
use App\Services\Auth\AuthService;
use App\Services\Auth\Dto\LoginDto;
use App\Services\Auth\Dto\RegisterUserData;
use Illuminate\Support\Facades\Facade;

/**
 * @method static AuthUserResource store(RegisterUserData $data)
 * @method static array login(LoginDto $data)
 *
 * @see AuthService
 */
class AuthFacade extends Facade
{
    public static function getFacadeAccessor(): string
    {
        return AuthService::class;
    }
}
