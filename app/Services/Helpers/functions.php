<?php

declare(strict_types=1);

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

function responseOk(): JsonResponse
{
    return response()->json([
        'status' => 'success',
    ]);
}

function responseFailed(string $message = null, int $code = Response::HTTP_BAD_REQUEST): JsonResponse
{
    return response()->json([
        'message' => $message,
    ], $code);
}

function query(array $array, string $key, int|string $default = null): ?string
{
    return strval(array_key_exists($key, $array) ? $array[$key] : $default);
}

function getMessage(string $code = null): ?string
{
    return __(sprintf('messages.%s', $code));
}
