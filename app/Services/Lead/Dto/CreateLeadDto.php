<?php

declare(strict_types=1);

namespace App\Services\Lead\Dto;

use Spatie\LaravelData\Data;

class CreateLeadDto extends Data
{
    public string $message;
}
