<?php

declare(strict_types=1);

namespace App\Services\Lead\Dto;

use App\Enums\LeadStatus;
use Spatie\LaravelData\Data;

class UpdateLeadDto extends Data
{
    public string $id;

    public string $comment;

    public LeadStatus $status;
}
