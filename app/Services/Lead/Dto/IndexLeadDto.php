<?php

declare(strict_types=1);

namespace App\Services\Lead\Dto;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Spatie\LaravelData\Data;

class IndexLeadDto extends Data
{
    public int $countRecords;

    public int $limit;

    public int $offset;

    public AnonymousResourceCollection $leads;
}
