<?php

namespace App\Services\Lead;

use App\Events\UserAddCommentEvent;
use App\Http\Resources\Lead\LeadResource;
use App\Models\Lead;
use App\Services\Lead\Dto\CreateLeadDto;
use App\Services\Lead\Dto\IndexLeadDto;
use App\Services\Lead\Dto\UpdateLeadDto;
use Illuminate\Support\Facades\DB;

class LeadService
{
    public function store(CreateLeadDto $data): Lead
    {
        $user = auth()->user();

        /** @var Lead $lead */
        $lead = $user->leads()->create($data->toArray());

        return Lead::query()->find($lead->id);
    }

    public function index(array $queryString): IndexLeadDto
    {
        $allowedFilterFields = (new Lead())->getFillable();
        $allowedSortFields = ['id', ...$allowedFilterFields];

        $collection = Lead::query()
            ->select($allowedSortFields)
            ->with('user:id,name,email,type');

        foreach ($allowedFilterFields as $key) {
            if ($paramFilter = query($queryString, '_'.$key)) {
                $paramFilter = preg_replace('#([%_?+])#', '\$1', $paramFilter);
                $collection->where($key, $paramFilter);
            }
        }

        $countOfRecords = $collection->count();

        $limit = intval(query($queryString, 'limit', 20));
        $limit = min($limit, 20);
        $collection->limit($limit);
        $offset = intval(query($queryString, 'offset', 0));
        $offset = max($offset, 0);
        $collection->offset($offset);

        return IndexLeadDto::from([
            'countRecords' => $countOfRecords,
            'limit' => $limit,
            'offset' => $offset,
            'leads' => LeadResource::collection($collection->get()),
        ]);
    }

    public function update(UpdateLeadDto $data): Lead
    {
        /** @var Lead $lead */
        $lead = Lead::query()->find($data->id);

        if (! $this->isSameDataInLead($lead, $data)) {
            $lead->comment = $data->comment;
            $lead->status = $data->status->value;

            DB::transaction(function () use ($lead) {
                $lead->save();
            });

            UserAddCommentEvent::dispatch($lead->comment, $lead->user->email);
        }

        return $lead;
    }

    private function isSameDataInLead(Lead $lead, UpdateLeadDto $data): bool
    {
        return $lead->comment === $data->comment && $lead->status === $data->status;
    }
}
