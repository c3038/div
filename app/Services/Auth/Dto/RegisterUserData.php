<?php

namespace App\Services\Auth\Dto;

use App\Enums\UserType;
use Illuminate\Support\Optional;
use Spatie\LaravelData\Data;

class RegisterUserData extends Data
{
    public UserType|Optional $type;

    public function __construct(
        public string $name,
        public string $email,
        public string $password,
    ) {
        $this->type = UserType::User;
    }
}
