<?php

declare(strict_types=1);

namespace App\Services\Auth;

use App\Enums\UserType;
use App\Exceptions\Auth\InvalidCredentialsException;
use App\Http\Resources\Auth\AuthUserResource;
use App\Models\User;
use App\Services\Auth\Dto\LoginDto;
use App\Services\Auth\Dto\RegisterUserData;
use Laravel\Sanctum\NewAccessToken;

class AuthService
{
    public function store(RegisterUserData $data): AuthUserResource
    {
        return new AuthUserResource(User::query()->create($data->toArray()));
    }

    /**
     * @throws InvalidCredentialsException
     */
    public function login(LoginDto $data): array
    {
        if (! auth()->guard('web')->attempt($data->toArray())) {
            throw new InvalidCredentialsException();
        }

        /** @var NewAccessToken $token */
        $token = auth()->user()->createToken(config('auth.token_secret'), [$this->setPermission()]);

        return [
            'token' => $token->plainTextToken,
        ];
    }

    private function setPermission(): string
    {
        return match (auth()->user()->type) {
            UserType::Manager() => sprintf('is:%s', UserType::Manager()),
            default => '*',
        };
    }

    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }
}
