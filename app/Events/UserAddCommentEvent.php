<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserAddCommentEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public string $comment;

    public string $email;

    /**
     * Create a new event instance.
     */
    public function __construct(string $comment, string $email)
    {
        $this->comment = $comment;
        $this->email = $email;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new PrivateChannel('channel-name'),
        ];
    }
}
