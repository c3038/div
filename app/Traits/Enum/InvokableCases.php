<?php

declare(strict_types=1);

namespace App\Traits\Enum;

use LogicException;

/**
 * @method static \UnitEnum[] cases()
 */
trait InvokableCases
{
    public function __invoke(): mixed
    {
        return $this->value;
    }

    public static function __callStatic(string $name, array $arguments): mixed
    {
        foreach (static::cases() as $case) {
            if ($case->name === $name) {
                return $case->value;
            }
        }

        throw new LogicException(sprintf('Enum case with name "%s" not found', $name));
    }
}
