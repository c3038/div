<?php

namespace App\Listeners;

use App\Events\UserAddCommentEvent;
use App\Mail\AnswerInComment;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendCommentToEmailEventHandler //implements ShouldQueue
{
    //    public string $queue = 'Lead';
    //    public string $connection ='redis';

    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(UserAddCommentEvent $event): void
    {
        Mail::to($event->email)->send(new AnswerInComment($event->comment));
        Log::info(sprintf('EMAIL: %s comment: %s', $event->email, $event->comment));
    }
}
