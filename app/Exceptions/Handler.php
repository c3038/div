<?php

namespace App\Exceptions;

use App\Exceptions\Auth\InvalidCredentialsException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response as BaseResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //            return responseFailed($e->getMessage(), Response::HTTP_NOT_FOUND);
        });
    }

    public function render($request, Throwable $e): BaseResponse|JsonResponse|RedirectResponse|Response
    {
        if ($e instanceof ModelNotFoundException) {
            return responseFailed(getMessage('model_not_found'), Response::HTTP_NOT_FOUND);
        }

        $this->renderable(function (NotFoundHttpException $e) {
            return responseFailed(getMessage('bad_request'), Response::HTTP_NOT_FOUND);
        });

        $this->renderable(function (AuthenticationException|BindingResolutionException $e) {
            return responseFailed(getMessage('auth_error'), Response::HTTP_UNAUTHORIZED);
        });

        $this->renderable(function (InvalidCredentialsException $e) {
            return responseFailed(getMessage('invalid_credentials'));
        });

        return parent::render($request, $e);
    }
}
