<?php

namespace App\Exceptions\Auth;

use Exception;
use Throwable;

class InvalidCredentialsException extends Exception
{
    public function __construct(string $message = null, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message ?? getMessage('invalid_credentials'), $code, $previous);
    }
}
