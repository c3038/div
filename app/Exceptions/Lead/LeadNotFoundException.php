<?php

namespace App\Exceptions\Lead;

use Exception;

class LeadNotFoundException extends Exception
{
    public function __construct(string $message = null, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message ?? getMessage('lead_not_found'), $code, $previous);
    }
}
