<?php

declare(strict_types=1);

namespace App\Enums;

use App\Traits\Enum\InvokableCases;
use App\Traits\Enum\Values;

enum UserType: string
{
    use InvokableCases;
    use Values;

    case User = 'user';
    case Manager = 'manager';
}
