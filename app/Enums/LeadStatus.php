<?php

declare(strict_types=1);

namespace App\Enums;

use App\Traits\Enum\InvokableCases;
use App\Traits\Enum\Values;

enum LeadStatus: string
{
    use InvokableCases;
    use Values;

    case Active = 'active';
    case Resolved = 'resolved';
}
