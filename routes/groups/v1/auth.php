<?php

declare(strict_types=1);

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
    Route::controller(AuthController::class)->group(function () {
        Route::post('/register', 'register')
            ->name('register');
        Route::post('/login', 'login')
            ->name('login');
    });
});
