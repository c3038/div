<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1'], function () {
    require __DIR__.'/auth.php';
    require __DIR__.'/lead.php';
});
