<?php

declare(strict_types=1);

use App\Http\Controllers\Lead\LeadController;
use Illuminate\Support\Facades\Route;

Route::controller(LeadController::class)
    ->prefix('leads')
    ->as('leads.')
    ->group(function () {
        Route::post('/', 'store')->name('store');
        Route::get('/', 'index')->name('index');
        Route::get('/{lead}', 'show')->name('show');
        Route::patch('/{lead}', 'update')->name('update');
    });
