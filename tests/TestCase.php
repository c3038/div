<?php

namespace Tests;

use App\Enums\UserType;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Sanctum\Sanctum;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected User $manager;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->withHeader('Accept', 'application/json');
    }

    protected function signInManager(): void
    {
        $this->manager = User::factory()->createOne([
            'name' => 'manager',
            'email' => 'manager@mail.ru',
            'type' => UserType::Manager,
        ]);
        Sanctum::actingAs($this->manager, [sprintf('is:%s', UserType::Manager())]);
    }

    protected function signInUser(): void
    {
        $this->user = User::factory()->createOne();
        Sanctum::actingAs($this->user);
    }
}
