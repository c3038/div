<?php

namespace Tests\Feature\Lead;

use App\Models\Lead;
use Illuminate\Support\Arr;
use Tests\TestCase;

class CreateLeadTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->signInUser();
    }

    public function test_create_lead(): void
    {
        $data = [
            'message' => fake()->text(50),
        ];
        $response = $this->post(route('leads.store'), $data);
        $response->assertCreated();
        $response->assertJsonStructure([
            'id',
            'userName',
            'email',
            'type',
            'status',
            'message',
            'comment',
            'created_at',
            'updated_at',
        ]);

        $response->assertJson([
            'message' => Arr::get($data, 'message'),
        ]);

        $this->assertDatabaseHas(Lead::class, [
            'id' => $response->json('id'),
            'user_id' => $this->user->id,
            'status' => $response->json('status'),
            'message' => Arr::get($data, 'message'),
            'comment' => $response->json('comment'),
        ]);
    }

    public function test_create_lead_failed_validation(): void
    {
        $data = [
            'message' => '',
        ];

        $response = $this->post(route('leads.store'), $data);

        $response->assertBadRequest();
        $response->assertJsonValidationErrors(['message']);
    }
}
