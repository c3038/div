<?php

namespace Tests\Feature\Lead;

use App\Models\Lead;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GetLeadsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signInManager();

        Lead::factory()
            ->count(rand(1, 4))
            ->for($this->manager)
            ->create();
    }

    public function test_get_leads(): void
    {
        $response = $this->get(route('leads.index'));

        $response->assertOk();
        $response->assertJsonStructure([
            'countRecords',
            'limit',
            'offset',
            'leads' => [
                '*' => [
                    'id',
                    'userName',
                    'email',
                    'type',
                    'status',
                    'message',
                    'comment',
                    'created_at',
                    'updated_at',
                ],
            ],
        ]);
    }
}
