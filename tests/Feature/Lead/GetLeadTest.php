<?php

namespace Tests\Feature\Lead;

use App\Models\Lead;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GetLeadTest extends TestCase
{
    use RefreshDatabase;

    private Lead $lead;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signInManager();

        $this->lead = Lead::factory()
            ->for($this->manager)
            ->create();
    }

    public function test_get_lead(): void
    {

        $response = $this->get(route('leads.show', ['lead' => $this->lead->id]));

        $response->assertOk();
        $response->assertJsonStructure([
            'id',
            'userName',
            'email',
            'type',
            'status',
            'message',
            'comment',
            'created_at',
            'updated_at',
        ]);
    }
}
