<?php

namespace Tests\Feature\Lead;

use App\Enums\LeadStatus;
use App\Models\Lead;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Arr;
use Tests\TestCase;

class UpdateLeadTest extends TestCase
{
    use RefreshDatabase;

    private Lead $lead;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signInManager();

        $this->lead = Lead::factory()
            ->for($this->manager)
            ->create();
    }

    public function test_update_lead(): void
    {
        $data = [
            'comment' => 'New comment',
            'status' => LeadStatus::Resolved(),
        ];

        $response = $this->patch(route('leads.update', ['lead' => $this->lead->id]), $data);

        $response->assertOk();
        $response->assertJsonStructure([
            'id',
            'userName',
            'email',
            'type',
            'status',
            'message',
            'comment',
            'created_at',
            'updated_at',
        ]);
        $response->assertJson([
            'comment' => Arr::get($data, 'comment'),
            'status' => Arr::get($data, 'status'),
        ]);
        $this->assertDatabaseHas(Lead::class, [
            'id' => $this->lead->id,
            'comment' => Arr::get($data, 'comment'),
            'status' => Arr::get($data, 'status'),
        ]);
    }
}
