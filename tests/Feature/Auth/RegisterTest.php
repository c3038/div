<?php

namespace Tests\Feature\Auth;

use App\Enums\UserType;
use App\Models\User;
use Illuminate\Support\Arr;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    public function test_success_register(): void
    {
        $data = [
            'name' => fake()->name,
            'email' => fake()->unique()->email,
            'password' => 'password',
            'password_confirmation' => 'password',
        ];

        $response = $this->post(route('auth.register'), $data);

        $response->assertCreated();
        $response->assertJsonStructure([
            'id',
            'name',
            'email',
            'type',
        ]);
        $response->assertJson([
            'name' => Arr::get($data, 'name'),
            'email' => Arr::get($data, 'email'),
            'type' => UserType::User(),
        ]);

        $this->assertDatabaseHas(User::class, [
            'id' => $response->json('id'),
            'name' => Arr::get($data, 'name'),
            'email' => Arr::get($data, 'email'),
            'type' => UserType::User(),
        ]);
    }

    public function test_register_validation(): void
    {
        $response = $this->post(route('auth.register'), [
            'name' => '',
            'email' => 'vasiamail.ru',
            'password' => '123123123',
            'password_confirmation' => '123',
        ]);

        $response->assertUnprocessable();
        $response->assertJsonValidationErrors([
            'name',
            'email',
            'password',
        ]);
    }
}
