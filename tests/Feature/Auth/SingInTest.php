<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Tests\TestCase;

class SingInTest extends TestCase
{
    private User $userWithoutAuth;

    protected function setUp(): void
    {
        parent::setUp();

        $this->userWithoutAuth = User::factory()->create();
    }

    public function test_success_auth_with_email(): void
    {
        $response = $this->post(route('auth.login'), [
            'email' => $this->userWithoutAuth->email,
            'password' => 'password',
        ]);

        $response->assertOk();

        $response->assertJsonStructure(['token']);
    }

    public function test_login_email_field_validation(): void
    {
        $response = $this->post(route('auth.login'), [
            'email' => 'vasiamail.ru',
            'password' => '',
        ]);

        $response->assertBadRequest();
        $response->assertJsonValidationErrors([
            'email',
            'password',
        ]);
    }
}
