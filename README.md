## Lead service

Система принятия и обработки заявок пользователей с сайта.
Любой пользователь может отправить данные по публичному API,
оставив заявку с каким-то текстом.
Затем заявка рассматривается ответственным лицом(менеджером) и ей
устанавливается статус Завершено.
Чтобы установить этот статус, ответственное лицо должно
оставить комментарий.
Пользователь должен получить свой ответ по email.

При этом, ответственное лицо должно иметь возможность
получить список заявок, отфильтровать их по статусу,
а также иметь возможность ответить задающему вопрос
через email.

### Endpoint`ы API:

Base URL: **/api/v1**

### **GET** /leads?_status=resolved

- получение заявок ответственным лицом, с фильтрацией по статусу

### **PATCH** /leads/{lead}

- ответ на конкретную задачу ответственным лицом

### **GET** /leads/{lead}

- просмотр задачу ответственным лицом

### **POST** /leads

- отправка заявки пользователями системы

Проверить работоспособность системы можно воспользовавшись функционалом swagger по адресу
http://localhost:8000/api/documentation

## Установка

```bash
git clone https://gitlab.com/c3038/div.git
```

### Docker

Linux

```bash***
make build
```

Windows

```bash
        cp .env.example .env
	docker-compose build
	docker-compose up -d
	docker-compose exec app composer install
	docker-compose exec app php artisan key:generate
	docker-compose exec app php artisan migrate
	docker-compose exec app php artisan db:seed
	docker-compose exec app php artisan l5-swagger:generate
	docker-compose exec app php artisan event:clear
	docker-compose exec app php artisan event:cache
```


